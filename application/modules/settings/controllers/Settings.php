<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    use PHPMailer\PHPMailer\Exception;
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    
    use sendotp\sendotp;

    require_once "vendor/autoload.php";

    class Settings extends MY_Controller{

        public function __construct(){
            parent::__construct();
        }

        public function isLoggedIn(){
            
            $isLoggedIn = $this->session->userdata ( 'b2cData' )['isLoggedIn'];
            if (! isset ( $isLoggedIn ) || $isLoggedIn != TRUE) {
                return false;
                redirect ( base_url() );
            } else {
                return true;
            }        
        }

        public function isLoggedOut(){
            if($this->session->has_userdata('access_token')){
                $this->session->unset_userdata('access_token');
            }
            $this->session->unset_userdata('b2cData');
            redirect ( base_url() );
        }

        public function getClient()
        {
            $clientDetails = [];
            $client = new Google_Client();
            $client->setAuthConfig(APPPATH. '/config/google_secret.json');
            $client->setApplicationName('Magnox Academy');
            $client->setRedirectUri(site_url('google_login'));
            $client->addScope('email');
            $client->addScope('profile');
            $client->addScope(Google_Service_Calendar::CALENDAR);
            $client->setAccessType('offline');
            $client->setPrompt('select_account consent');

            // $tokenPath = FCPATH.'uploads/token.json';
            
            // if (file_exists($tokenPath)) {
            //     $accessToken = json_decode(file_get_contents($tokenPath), true);
            //     $client->setAccessToken($accessToken);
            //     $clientDetails['client'] = $client;
            //     $clientDetails['status'] = 'valid';
            // }
            if ($client->isAccessTokenExpired()) {
                if ($client->getRefreshToken()) {
                    $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                    $clientDetails['client'] = $client;
                    $clientDetails['status'] = 'valid';
                } else {
                    // Request authorization from the user.
                    $authUrl = $client->createAuthUrl();
                    $clientDetails['client'] = "";
                    $clientDetails['status'] = 'invalid';
                    $clientDetails['authurl'] = $authUrl;
                }
            }
            return $clientDetails;
        }

        /*============================================================================================*/

        public function mobile_otp_send($phone, $code)
        {
            $otp = new sendotp( '220681Ap3YnI61Qsx5b237012','Your otp is {{otp}}. Please do not share with anyone.');
            $result = $otp->send($phone, 'MSGIND', $code);
            if($result['type'] === 'success'){
                return true;
            }else{
                return false;
            }
        }

        /*============================================================================================*/

        public function MailSystem($to, $cc, $subject, $message)
        {
            $email = 'magnox.tech@gmail.com';
            $password = 'Jahir#12345';

            $mail = new PHPMailer();
            //Enable SMTP debugging.
            $mail->SMTPDebug = 0;                               
            //Set PHPMailer to use SMTP.
            $mail->isSMTP();            
            //Set SMTP host name  
            $mail->CharSet = 'utf-8';// set charset to utf8
            //$mail->Encoding = 'base64';
            $mail->SMTPAuth = true;// Enable SMTP authentication
            $mail->SMTPSecure = 'ssl';// Enable TLS encryption, `ssl` also accepted

            $mail->Host = 'smtp.gmail.com';// Specify main and backup SMTP servers
            $mail->Port = 465;// TCP port to connect to
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );                           
            //Provide username and password     
            $mail->Username = $email;                 
            $mail->Password = $password;                                                                         

            $mail->From = $email;
            $mail->FromName = 'Magnox Academy';

            $mail->addAddress($to);

            $mail->isHTML(true);

            $mail->Subject = $subject;
            $mail->Body = $message;
            //$mail->AltBody = "This is the plain text version of the email content";
            return ($mail->send())? 1 : 0;
            //return ($mail->send())? 1 : $mail->ErrorInfo;
        }

    }//class ends
?>