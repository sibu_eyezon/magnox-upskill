<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Instructor extends MY_Controller{
        
        public function __construct(){
            parent::__construct();
            $this->load->module('lmssetup');
        }

        public function index(){
            $data = array();
            $data['pageTitle'] = 'Instructor Dashboard | Magnox Academy';
            $data['page_content'] = $this->load->view('instructor/dashboard', null, TRUE);
            $this->lmssetup->lmsmain($data);
        }

    }//class ends
?>