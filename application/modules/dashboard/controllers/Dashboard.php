<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    require_once "vendor/autoload.php";

    class Dashboard extends MY_Controller{
        
        public function __construct(){
            parent::__construct();
            $this->load->module('settings');
            $this->load->module('homesetup');
            $this->load->model('mastermodel', 'mm');
        }

        public function index(){
            $data = array();
            $data['pageTitle'] = 'Dashboard | Magnox Academy';
            $data['page_content'] = $this->load->view('dashboard/dashboard', null, TRUE);
            $this->homesetup->main($data);
        }
    }//class ends
?>