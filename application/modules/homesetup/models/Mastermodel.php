<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Mastermodel extends CI_Model{

        public function get_role_id_by_name($rolename)
        {
            $this->db->where('rolename',$rolename);
            return $this->db->get('master_role')->result();
        }

        public function check_unique_user($where)
        {
            $this->db->where($where);
            return $this->db->get('user_auth')->num_rows();
        }

        public function find_user_by_email($email)
        {
            $this->db->where('txt_email',$email);
            return $this->db->get('user_auth')->result();
        }

        public function get_user_roles($user_id)
        {
            $this->db->select('rolename, status')->from('master_role mr');
            $this->db->join('user_map_role umr', 'umr.role_id=mr.id', 'inner');
            $this->db->where(array('umr.user_id'=>$user_id, 'status<>'=>'N'));
            $this->db->order_by('mr.id', 'ASC');
            return $this->db->get()->result();
        }

        public function setup_user_data($udetails, $uauth, $urole)
        {
            $this->db->trans_off();
            $this->db->trans_begin();

            $user_id = insertDataRetId('user_details', $udetails);
            $urole['user_id'] = $uauth['user_id'] = $user_id;
            insertData('user_auth', $uauth);
            insertData('user_map_role', $urole);

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                return false;
            }
            else
            {
                $this->db->trans_commit();
                return $user_id;
            }
        }

    }//class ends
?>