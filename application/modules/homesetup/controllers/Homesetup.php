<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Homesetup extends MY_Controller{
        
        public function __construct(){
            parent::__construct();
        }

        public function main($data)
        {
            $this->load->view('index', $data);
        }

    }//class ends
?>