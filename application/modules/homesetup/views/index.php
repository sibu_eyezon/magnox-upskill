<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8" />
		<meta name="author" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= $pageTitle; ?></title>
        <meta name="google-signin-client_id" content="<?= (isset($client_id))? $client_id : ''; ?>">
        <link href="<?= site_url(); ?>public/css/styles.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Material+Icons" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="<?= site_url(); ?>public/lms/bundles/izitoast/css/iziToast.min.css">
		<link rel='shortcut icon' type='image/x-icon' href='<?= site_url(); ?>public/images/favicon.ico' />
		<script src="<?= site_url(); ?>public/js/jquery.min.js"></script>
		<script src="<?= site_url(); ?>public/js/popper.min.js"></script>
		<script src="<?= site_url(); ?>public/js/bootstrap.min.js"></script>
		<script src="<?= site_url(); ?>public/lms/bundles/izitoast/js/iziToast.min.js"></script>
		<script src="<?= site_url(); ?>public/lms/bundles/sweetalert/sweetalert.min.js"></script>
		<script src="<?= site_url(); ?>public/lms/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
		<script type="text/javascript">var baseURL = '<?= site_url(); ?>';</script>
    </head>
	
    <body>
    	<script>
		    function show_pop_messages(msg, status){
		      if(status){
		        iziToast.success({
		          title: 'Success!',
		          message: msg,
		          position: 'topRight'
		        });
		      }else{
		        iziToast.error({
		          title: 'Warning!',
		          message: msg,
		          position: 'topRight'
		        });
		      }
		    }
		  </script>
		  <?php 
		    if($this->session->flashdata('errors')!=""){
		      echo '<script>show_pop_messages("'.$this->session->flashdata("errors").'", false);</script>';
		      unset($_SESSION['errors']);
		    }else if($this->session->flashdata('success')!=""){
		      echo '<script>show_pop_messages("'.$this->session->flashdata("success").'", true);</script>';
		      unset($_SESSION['success']);
		    }
		  ?>
        <div id="main-wrapper">
        	<?php include 'header.php'; ?>

        	<?php if(isset($page_content)){ echo $page_content; } ?>

        	<?php include 'footer.php'; ?>

        	<?php include 'modals.php'; ?>
        	<a id="back2Top" class="top-scroll" title="Back to top" href="#"><i class="ti-arrow-up"></i></a>
        </div>
		<script src="<?= site_url(); ?>public/js/select2.min.js"></script>
		<script src="<?= site_url(); ?>public/js/slick.js"></script>
		<script src="<?= site_url(); ?>public/js/moment.min.js"></script>
		<script src="<?= site_url(); ?>public/js/daterangepicker.js"></script> 
		<script src="<?= site_url(); ?>public/js/summernote.min.js"></script>
		<script src="<?= site_url(); ?>public/js/metisMenu.min.js"></script>	
		<script src="<?= site_url(); ?>public/js/custom.js"></script>
		<script src="<?= site_url(); ?>public/js/raphael.min.js"></script>
		<script src="<?= site_url(); ?>public/js/morris.min.js"></script>
		<script src="<?= site_url(); ?>public/js/morris.js"></script>
    </body>

</html>