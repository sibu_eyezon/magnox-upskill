<div class="header header-light">
	<div class="container">
		<nav id="navigation" class="navigation navigation-landscape">
			<div class="nav-header">
				<a class="nav-brand" href="<?= site_url(); ?>">
					<img src="<?= site_url(); ?>public/images/logo.png" class="logo" alt="" />
				</a>
				<div class="nav-toggle"></div>
				<div class="mobile_nav">
					<ul>
						<?php if(!$this->session->has_userdata('b2cData')){ ?>
						<li>
							<a href="<?= site_url('login'); ?>" class="alio_green">
								<i class="fas fa-sign-in-alt mr-1"></i><span class="dn-lg">Log In</span>
							</a>
						</li>
						<?php }else{ ?>
						<li class="account-drop">
							<a href="javascript:void(0);" class="crs_yuo12" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="embos_45"><i class="fas fa-shopping-basket"></i><i class="embose_count">4</i></span>
							</a>
							<div class="dropdown-menu pull-right animated flipInX">
								<div class="drp_menu_headr bg-purple">
									<h4>Wishlist Product</h4>
								</div>
								<div class="ground-list ground-hover-list">
									<div class="ground ground-list-single">
										<div class="grd_thum"><img src="assets/img/cr-1.jpg" class="img-fluid rounded" width="50" alt="" /></div>
										<div class="ground-content">
											<h6>Web History<small class="float-right text-fade">$120</small></h6>
											<a href="#" class="small text-danger">Remove</a>
										</div>
									</div>
									
									<div class="ground ground-list-single">
										<button type="button" class="btn theme-bg text-white full-width">Go To Cart</button>
									</div>
									
								</div>
							</div>
						</li>
						<li class="account-drop">
							<a href="javascript:void(0);" class="crs_yuo12" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="embos_45"><i class="fas fa-bell"></i><i class="embose_count red">3</i></span>
							</a>
							<div class="dropdown-menu pull-right animated flipInX">
								<div class="drp_menu_headr bg-warning">
									<h4>22 Notifications</h4>
								</div>
								<div class="ground-list ground-hover-list">
									<div class="ground ground-list-single">
										<div class="rounded-circle p-3 p-sm-4 d-flex align-items-center justify-content-center bg-light-success">
											<div class="position-absolute text-success h5 mb-0"><i class="fas fa-user"></i></div>
										</div>

										<div class="ground-content">
											<h6><a href="#">Maryam Amiri</a></h6>
											<small class="text-fade">New User Enrolled in Python</small>
											<span class="small">Just Now</span>
										</div>
									</div>
									
								</div>
							</div>
						</li>						
						<li>
							<div class="btn-group account-drop">
								<a href="javascript:void(0);" class="crs_yuo12 btn btn-order-by-filt" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<img src="<?= $_SESSION['b2cData']['profile_img']; ?>" class="avater-img" alt="">
								</a>
								<div class="dropdown-menu pull-right animated flipInX">
									<div class="drp_menu_headr">
										<h4>Hi, <?= $_SESSION['b2cData']['name']; ?></h4>
									</div>
									<ul>
										<li><a href="<?= site_url('dashboard'); ?>"><i class="fas fa-tachometer-alt"></i>Dashboard<span class="notti_coun style-1">4</span></a></li>                                  
										<li><a href="<?= site_url('profile'); ?>"><i class="fas fa-user-tie"></i>My Profile</a></li>      
										<li><a href="<?= site_url('my-learning'); ?>"><i class="fas fa-shopping-basket"></i>My Learning<span class="notti_coun style-2">7</span></a></li>
										<?php if(isset($_SESSION['roles'][1])){ 
											$istat = $_SESSION['roles'][1]['status'];
										?>
										<li><a href="<?= site_url('instructor'); ?>"<?php if($istat == 'B'){ echo 'disabled'; } ?>><i class="fas fa-toolbox"></i>Instructor Dashboard</a> <?php if($istat == 'B'){ echo '<span class="text-danger">Blocked<span>'; } ?> </li>
										<?php } ?>
										<li><a href="#"><i class="fas fa-comments"></i>Notifications</a></li>
										<li><a href="#"><i class="fas fa-comments"></i>Messages</a></li>
										<li><a href="#"><i class="fas fa-comments"></i>Wishlist</a></li>
										<li><a href="#"><i class="fas fa-comments"></i>Purchase History</a></li>
										<li><a href="<?= site_url('logout'); ?>"><i class="fa fa-unlock-alt"></i>Sign Out</a></li>
									</ul>
								</div>
							</div>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<div class="nav-menus-wrapper" style="transition-property: none;">
				<ul class="nav-menu">
				
					<li><a href="#">Explore Programs</a>
					</li>
					
					<li class=""><a href="#">Career Support</a>
					</li>
					
					<li class=""><a href="#">Instructors</a>
					</li>

					<li class=""><a href="#">More<span class="submenu-indicator"></span></a>
						<ul class="nav-dropdown nav-submenu">
							<li><a href="#">Blog</a></li>
							<li><a href="#">Success Stories</a></li>
							<li><a href="#">Gallery</a></li>
							<li><a href="#">Become Business Associate</a></li>
							<li><a href="#">Become Client Associate</a></li>
							<li><a href="#">Collaborate Now</a></li>
							<li><a href="#">Contact Us</a></li>
						</ul>
					</li>
					
				</ul>
				
				<ul class="nav-menu nav-menu-social align-to-right">
					<?php if(!$this->session->has_userdata('b2cData')){ ?>
					<li>
						<a href="<?= site_url('login'); ?>" class="alio_green">
							<i class="fas fa-sign-in-alt mr-1"></i><span class="dn-lg">Sign In</span>
						</a>
					</li>
					<li class="add-listing theme-bg">
						<a href="#" class="text-white">Get Started</a>
					</li>
					<?php }else{ ?>
					<li class="account-drop">
						<a href="javascript:void(0);" class="crs_yuo12" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="embos_45"><i class="fas fa-shopping-basket"></i><i class="embose_count">4</i></span>
						</a>
						<div class="dropdown-menu pull-right animated flipInX">
							<div class="drp_menu_headr bg-purple">
								<h4>Wishlist Product</h4>
							</div>
							<div class="ground-list ground-hover-list">
								<div class="ground ground-list-single">
									<div class="grd_thum"><img src="assets/img/cr-1.jpg" class="img-fluid rounded" width="50" alt="" /></div>
									<div class="ground-content">
										<h6>Web History<small class="float-right text-fade">$120</small></h6>
										<a href="#" class="small text-danger">Remove</a>
									</div>
								</div>
								
								<div class="ground ground-list-single">
									<div class="grd_thum"><img src="assets/img/cr-3.jpg" class="img-fluid rounded" width="50" alt="" /></div>
									<div class="ground-content">
										<h6>Physics Beginning<small class="float-right text-fade">$99</small></h6>
										<a href="#" class="small text-danger">Remove</a>
									</div>
								</div>
								
								<div class="ground ground-list-single">
									<div class="grd_thum"><img src="assets/img/cr-4.jpg" class="img-fluid rounded" width="50" alt="" /></div>
									<div class="ground-content">
										<h6>Computer Fundamental<small class="float-right text-fade">$99</small></h6>
										<a href="#" class="small text-danger">Remove</a>
									</div>
								</div>
								
								<div class="ground ground-list-single">
									<div class="grd_thum"><img src="assets/img/cr-5.jpg" class="img-fluid rounded" width="50" alt="" /></div>
									<div class="ground-content">
										<h6>Computer Advance<small class="float-right text-fade">$49</small></h6>
										<a href="#" class="small text-danger">Remove</a>
									</div>
								</div>
								
								<div class="ground ground-list-single">
									<button type="button" class="btn theme-bg text-white full-width">Go To Cart</button>
								</div>
								
							</div>
						</div>
					</li>
					<li class="account-drop">
						<a href="javascript:void(0);" class="crs_yuo12" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="embos_45"><i class="fas fa-bell"></i><i class="embose_count red">3</i></span>
						</a>
						<div class="dropdown-menu pull-right animated flipInX">
							<div class="drp_menu_headr bg-warning">
								<h4>22 Notifications</h4>
							</div>
							<div class="ground-list ground-hover-list">
								<div class="ground ground-list-single">
									<div class="rounded-circle p-3 p-sm-4 d-flex align-items-center justify-content-center bg-light-success">
										<div class="position-absolute text-success h5 mb-0"><i class="fas fa-user"></i></div>
									</div>

									<div class="ground-content">
										<h6><a href="#">Maryam Amiri</a></h6>
										<small class="text-fade">New User Enrolled in Python</small>
										<span class="small">Just Now</span>
									</div>
								</div>
								
								<div class="ground ground-list-single">
									<div class="rounded-circle p-3 p-sm-4 d-flex align-items-center justify-content-center bg-light-danger">
										<div class="position-absolute text-danger h5 mb-0"><i class="fas fa-comments"></i></div>
									</div>

									<div class="ground-content">
										<h6><a href="#">Shilpa Rana</a></h6>
										<small class="text-fade">Shilpa Send a Message</small>
										<span class="small">02 Min Ago</span>
									</div>
								</div>
								
								<div class="ground ground-list-single">
									<div class="rounded-circle p-3 p-sm-4 d-flex align-items-center justify-content-center bg-light-info">
										<div class="position-absolute text-info h5 mb-0"><i class="fas fa-grin-squint-tears"></i></div>
									</div>

									<div class="ground-content">
										<h6><a href="#">Amar Muskali</a></h6>
										<small class="text-fade">Need Responsive Business Tem...</small>
										<span class="small">10 Min Ago</span>
									</div>
								</div>
								
								<div class="ground ground-list-single">
									<div class="rounded-circle p-3 p-sm-4 d-flex align-items-center justify-content-center bg-light-purple">
										<div class="position-absolute text-purple h5 mb-0"><i class="fas fa-briefcase"></i></div>
									</div>

									<div class="ground-content">
										<h6><a href="#">Maryam Amiri</a></h6>
										<small class="text-fade">Next Meeting on Tuesday..</small>
										<span class="small">15 Min Ago</span>
									</div>
								</div>
								
							</div>
						</div>
					</li>
					<li>
						<div class="btn-group account-drop">
							<a href="javascript:void(0);" class="crs_yuo12 btn btn-order-by-filt" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<img src="<?= $_SESSION['b2cData']['profile_img']; ?>" class="avater-img" alt="">
							</a>
							<div class="dropdown-menu pull-right animated flipInX">
								<div class="drp_menu_headr">
									<h4>Hi, <?= $_SESSION['b2cData']['name']; ?></h4>
								</div>
								<ul>
									<li><a href="<?= site_url('dashboard'); ?>"><i class="fas fa-tachometer-alt"></i>Dashboard</a></li>                                  
									<li><a href="<?= site_url('profile'); ?>"><i class="fas fa-user-tie"></i>My Profile</a></li>      
									<li><a href="<?= site_url('my-learning'); ?>"><i class="fas fa-shopping-basket"></i>My Learning<span class="notti_coun style-2">7</span></a></li>
									<?php if(isset($_SESSION['roles'][1])){ 
										$istat = $_SESSION['roles'][1]['status'];
									?>
									<li><a href="<?= site_url('instructor'); ?>"<?php if($istat == 'B'){ echo 'disabled'; } ?>><i class="fas fa-toolbox"></i>Instructor Dashboard</a> <?php if($istat == 'B'){ echo '<span class="text-danger">Blocked<span>'; } ?> </li>
									<?php } ?>
									<li><a href="#"><i class="fas fa-comments"></i>Notifications</a></li>
									<li><a href="#"><i class="fas fa-comments"></i>Messages</a></li>
									<li><a href="#"><i class="fas fa-comments"></i>Wishlist</a></li>
									<li><a href="#"><i class="fas fa-comments"></i>Purchase History</a></li>
									<li><a href="<?= site_url('logout'); ?>"><i class="fa fa-unlock-alt"></i>Sign Out</a></li>
								</ul>
							</div>
						</div>
					</li>
					<?php } ?>
				</ul>
			</div>
		</nav>
	</div>
	</div>
	<!-- End Navigation -->
	<div class="clearfix"></div>