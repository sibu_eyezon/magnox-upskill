<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Student extends MY_Controller{
        
        public function __construct(){
            parent::__construct();
            $this->load->module('homesetup');
        }

        public function index(){
            $data = array();
            $data['pageTitle'] = 'My Learning | Magnox Academy';
            $data['page_content'] = $this->load->view('student/dashboard', null, TRUE);
            $this->homesetup->main($data);
        }

    }//class ends
?>