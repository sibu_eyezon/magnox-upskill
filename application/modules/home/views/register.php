<section>
	<div class="container">
		<div class="row justify-content-center">
		
			<div class="col-xl-7 col-lg-8 col-md-12 col-sm-12">
				<form method="POST" id="frmRegister">
					<div class="crs_log_wrap">
						<div class="crs_log__thumb">
							<img src="<?= site_url('public/images/banner-2.jpg'); ?>" class="img-fluid" alt="" />
						</div>
						<div class="crs_log__caption">
							<div class="rcs_log_123">
								<div class="rcs_ico"><i class="fas fa-lock"></i></div>
							</div>
							
							<div class="rcs_log_124">
								<div class="Lpo09"><h4>Create Your Account</h4></div>
								<div class="form-group row mb-0">
									<div class="col-xl-6 col-lg-12 col-md-12 col-sm-12">
										<div class="form-group">
											<label>First Name</label>
											<input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" />
										</div>
									</div>
									<div class="col-xl-6 col-lg-12 col-md-12 col-sm-12">
										<div class="form-group">
											<label>Last Name</label>
											<input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" />
										</div>
									</div>
								</div>

								<div class="form-group row mb-0">
									<div class="col-xl-6 col-lg-12 col-md-12 col-sm-12">
										<div class="form-group">
											<label>Email</label>
											<input type="email" class="form-control" name="email" id="email" placeholder="Email Address" />
										</div>
									</div>
									<div class="col-xl-6 col-lg-12 col-md-12 col-sm-12">
										<div class="form-group">
											<label>Mobile</label>
											<input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile Number" />
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<label>Password</label>
									<div class="input-with-icon">
										<input type="password" class="form-control" name="password" id="password" placeholder="*******">
										<i class="ti-unlock"></i>
									</div>
								</div>

								<div class="form-group">
									<label>Confirm Password</label>
									<div class="input-with-icon">
										<input type="password" class="form-control" name="cnf_password" id="cnf_password" placeholder="*******">
										<i class="ti-unlock"></i>
									</div>
								</div>
								
								<div class="form-group">
									<button type="submit" id="btn-register" class="btn btn-md full-width theme-bg text-white">Register</button>
								</div>
							</div>
							<div class="rcs_log_125">
								<span>Or Login with Social Info</span>
							</div>
							<div class="rcs_log_126 full">
								<ul class="social_log_45 row">
									<li class="col-xl-4 col-lg-4 col-md-4 col-4"><a href="javascript:void(0);" class="sl_btn"><i class="ti-facebook text-info"></i>Facebook</a></li>
									<li class="col-xl-4 col-lg-4 col-md-4 col-4"><a href="javascript:void(0);" id="btn-google" class="sl_btn"><i class="ti-google text-danger"></i>Google</a></li>
									<li class="col-xl-4 col-lg-4 col-md-4 col-4"><a href="javascript:void(0);" class="sl_btn"><i class="ti-mobile text-primary"></i>OTP Login</a></li>
								</ul>
							</div>
						</div>
						<div class="crs_log__footer d-flex justify-content-between">
							<div class="fhg_45"><p class="musrt">Already have account? <a href="<?= site_url('login'); ?>" class="theme-cl">Login</a></p></div>
							<div class="fhg_45"><p class="musrt"><a href="<?= site_url('forget-password'); ?>" class="text-danger">Forgot Password?</a></p></div>
						</div>
					</div>
				</form>
			</div>

		</div>
	</div>
</section>
<script src="<?= site_url(); ?>public/js/common.min.js"></script>