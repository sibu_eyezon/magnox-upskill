<section>
	<div class="container">
		<div class="row justify-content-center">
		
			<div class="col-xl-7 col-lg-8 col-md-12 col-sm-12">
				<form method="POST" id="frmForget">
					<div class="crs_log_wrap">
						<div class="crs_log__thumb">
							<img src="<?= site_url('public/images/banner-2.jpg'); ?>" class="img-fluid" alt="" />
						</div>
						<div class="crs_log__caption">
							<div class="rcs_log_123">
								<div class="rcs_ico"><i class="fas fa-lock"></i></div>
							</div>
							
							<div class="rcs_log_124">
								<div class="Lpo09"><h4>Forgot password</h4></div>
								<div class="form-group">
									<label>Enter Email</label>
									<input type="email" name="reg_email" id="reg_email" class="form-control" placeholder="support@magnoxacademy.com" />
								</div>
								<div class="form-group">
									<button type="submit" id="btn-forget" class="btn full-width btn-md theme-bg text-white">Forgot password</button>
								</div>
							</div>
						</div>
						<div class="crs_log__footer d-flex justify-content-between">
							<div class="fhg_45"><p class="musrt">Don't have account? <a href="<?= site_url('register'); ?>" class="theme-cl">SignUp</a></p></div>
						</div>
					</div>
				</form>

				<form method="POST" id="frmChange" style="display: none;">
					<div class="crs_log_wrap">
						<div class="crs_log__thumb">
							<img src="<?= site_url('public/images/banner-2.jpg'); ?>" class="img-fluid" alt="" />
						</div>
						<div class="crs_log__caption">
							<div class="rcs_log_123">
								<div class="rcs_ico"><i class="fas fa-lock"></i></div>
							</div>
							
							<div class="rcs_log_124">
								<div class="Lpo09"><h4>Forgot password</h4></div>
								<div class="form-group">
									<label>Enter OTP</label>
									<input type="text" name="otp" id="otp" class="form-control" />
									<input type="hidden" name="code" id="code" value=""/>
									<input type="hidden" name="tokenId" id="tokenId" value=""/>
								</div>
								<div class="form-group">
									<label>New Password</label>
									<input type="password" name="password" id="password" class="form-control" />
								</div>
								<div class="form-group">
									<label>Confirm Password</label>
									<input type="password" name="cnf_password" id="cnf_password" class="form-control" />
								</div>
								<div class="form-group">
									<button type="submit" id="btn-change" class="btn full-width btn-md theme-bg text-white">Change password</button>
								</div>
							</div>
						</div>
						<div class="crs_log__footer d-flex justify-content-between">
							<div class="fhg_45"><p class="musrt">Don't have account? <a href="<?= site_url('register'); ?>" class="theme-cl">SignUp</a></p></div>
						</div>
					</div>
				</form>
			</div>

		</div>
	</div>
</section>
<script src="<?= site_url(); ?>public/js/forget-password.min.js"></script>