<div class="hero_banner image-cover image_bottom h7_bg">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="simple-search-wrap text-left">
					<div class="hero_search-2">
						<div class="elsio_tag yellow">LISTEN TO OUR NEW ANTHEM</div>
						<h1 class="banner_title mb-4">The Best<br>e-Learning Cources For<br><span class="light">Better Future</span></h1>
						<p class="font-lg mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
						<div class="input-group simple_search">
							<i class="fa fa-search ico"></i>
							<input type="text" class="form-control" placeholder="Search Your Cources">
							<div class="input-group-append">
								<button class="btn theme-bg" type="button">Search</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="side_block extream_img">
					<div class="list_crs_img">
						<img src="<?= site_url(); ?>public/images/ic-1.png" class="img-fluid cirl animate-fl-y" alt="" />
						<img src="<?= site_url(); ?>public/images/ic-2.png" class="img-fluid arrow animate-fl-x" alt="" />
						<img src="<?= site_url(); ?>public/images/ic-3.png" class="img-fluid moon animate-fl-x" alt="" />
					</div>
					<img src="<?= site_url(); ?>public/images/side-2.png" class="img-fluid" alt="" />
				</div>
			</div>
		</div>
	</div>
</div>

<section class="p-0">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="crp_box ovr_top">
					<div class="row align-items-center m-0">
						<div class="col-xl-2 col-lg-3 col-md-2 col-sm-12">
							<div class="crt_169">
								<div class="crt_overt style_2"><h4>4.7</h4></div>
								<div class="crt_stion">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
								</div>
								<div class="crt_io90"><h6>3,272 Rating</h6></div>
							</div>
						</div>
						<div class="col-xl-10 col-lg-9 col-md-10 col-sm-12">
							<div class="part_rcp">
								<ul>
									<li>
										<div class="dro_140">
											<div class="dro_141"><i class="fas fa-layer-group"></i></div>
											<div class="dro_142"><h6>Best Online<br>Tutoring</h6></div>
										</div>
									</li>
									<li>
										<div class="dro_140">
											<div class="dro_141 st-1"><i class="fas fa-business-time"></i></div>
											<div class="dro_142"><h6>Fully Lifetime<br>Access</h6></div>
										</div>
									</li>
									<li>
										<div class="dro_140">
											<div class="dro_141 st-2"><i class="fas fa-user-shield"></i></div>
											<div class="dro_142"><h6>800k+ Enrolled<br>Students</h6></div>
										</div>
									</li>
									<li>
										<div class="dro_140">
											<div class="dro_141 st-3"><i class="fas fa-journal-whills"></i></div>
											<div class="dro_142"><h6>200+ Cources<br>Available</h6></div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
	
		<div class="row justify-content-center">
			<div class="col-lg-7 col-md-10 text-center">
				<div class="sec-heading center mb-4">
					<h2>Recent Listed <span class="theme-cl">Cources</span></h2>
					<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores</p>
				</div>
			</div>
		</div>
		
		<div class="row justify-content-center">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
				<div class="slide_items">
				
					<!-- Single Item -->
					<div class="lios_item">	
						<div class="crs_grid shadow_none brd">
							<div class="crs_grid_thumb">
								<a href="course-detail.html" class="crs_detail_link">
									<img src="assets/img/cr-2.jpg" class="img-fluid rounded" alt="" />
								</a>
								<div class="crs_video_ico">
									<i class="fa fa-play"></i>
								</div>
								<div class="crs_locked_ico">
									<i class="fa fa-lock"></i>
								</div>
							</div>
							<div class="crs_grid_caption">
								<div class="crs_flex">
									<div class="crs_fl_first">
										<div class="crs_cates cl_6"><span>Development</span></div>
									</div>
									<div class="crs_fl_last">
										<div class="crs_price"><h2><span class="currency">$</span><span class="theme-cl">149</span></h2></div>
									</div>
								</div>
								<div class="crs_title"><h4><a href="course-detail.html" class="crs_title_link">Basic knowledge about hodiernal bharat in history</a></h4></div>
								<div class="crs_info_detail">
									<ul>
										<li><i class="fa fa-clock text-danger"></i><span>02 hr 05 min</span></li>
										<li><i class="fa fa-video text-success"></i><span>17 Lectures</span></li>
									</ul>
								</div>
								<div class="preview_crs_info">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width:50%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
							<div class="crs_grid_foot">
								<div class="crs_flex">
									<div class="crs_fl_first">
										<div class="crs_tutor">
											<div class="crs_tutor_thumb"><a href="instructor-detail.html"><img src="assets/img/user-6.jpg" class="img-fluid circle" alt="" /></a></div>
										</div>
									</div>
									<div class="crs_fl_last">
										<div class="foot_list_info">
											<ul>
												<li><div class="elsio_ic"><i class="fa fa-user text-danger"></i></div><div class="elsio_tx">4.7k</div></li>
												<li><div class="elsio_ic"><i class="fa fa-eye text-success"></i></div><div class="elsio_tx">42.4k</div></li>
												<li><div class="elsio_ic"><i class="fa fa-star text-warning"></i></div><div class="elsio_tx">4.7</div></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<!-- Single Item -->
					<div class="lios_item">
						<div class="crs_grid shadow_none brd">
							<div class="crs_grid_thumb">
								<a href="course-detail.html" class="crs_detail_link">
									<img src="assets/img/cr-3.jpg" class="img-fluid rounded" alt="" />
								</a>
								<div class="crs_video_ico">
									<i class="fa fa-play"></i>
								</div>
								<div class="crs_locked_ico">
									<i class="fa fa-lock"></i>
								</div>
							</div>
							<div class="crs_grid_caption">
								<div class="crs_flex">
									<div class="crs_fl_first">
										<div class="crs_cates cl_5"><span>Finance</span></div>
									</div>
									<div class="crs_fl_last">
										<div class="crs_price"><h2><span class="currency">$</span><span class="theme-cl">99</span></h2></div>
									</div>
								</div>
								<div class="crs_title"><h4><a href="course-detail.html" class="crs_title_link">Advance PHP knowledge with laravel to make smart web</a></h4></div>
								<div class="crs_info_detail">
									<ul>
										<li><i class="fa fa-clock text-danger"></i><span>02 hr 47 min</span></li>
										<li><i class="fa fa-video text-success"></i><span>32 Lectures</span></li>
									</ul>
								</div>
								<div class="preview_crs_info">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width:70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
							<div class="crs_grid_foot">
								<div class="crs_flex">
									<div class="crs_fl_first">
										<div class="crs_tutor">
											<div class="crs_tutor_thumb"><a href="instructor-detail.html"><img src="assets/img/user-5.jpg" class="img-fluid circle" alt="" /></a></div>
										</div>
									</div>
									<div class="crs_fl_last">
										<div class="foot_list_info">
											<ul>
												<li><div class="elsio_ic"><i class="fa fa-user text-danger"></i></div><div class="elsio_tx">4.7k</div></li>
												<li><div class="elsio_ic"><i class="fa fa-eye text-success"></i></div><div class="elsio_tx">42.4k</div></li>
												<li><div class="elsio_ic"><i class="fa fa-star text-warning"></i></div><div class="elsio_tx">4.7</div></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<!-- Single Item -->
					<div class="lios_item">
						<div class="crs_grid shadow_none brd">
							<div class="crs_grid_thumb">
								<a href="course-detail.html" class="crs_detail_link">
									<img src="assets/img/cr-4.jpg" class="img-fluid rounded" alt="" />
								</a>
								<div class="crs_video_ico">
									<i class="fa fa-play"></i>
								</div>
								<div class="crs_locked_ico">
									<i class="fa fa-lock"></i>
								</div>
							</div>
							<div class="crs_grid_caption">
								<div class="crs_flex">
									<div class="crs_fl_first">
										<div class="crs_cates cl_4"><span>Banking</span></div>
									</div>
									<div class="crs_fl_last">
										<div class="crs_price"><h2><span class="currency">$</span><span class="theme-cl">49</span></h2></div>
									</div>
								</div>
								<div class="crs_title"><h4><a href="course-detail.html" class="crs_title_link">The complete accounting & bank financial course 2021</a></h4></div>
								<div class="crs_info_detail">
									<ul>
										<li><i class="fa fa-clock text-danger"></i><span>04 hr 10 min</span></li>
										<li><i class="fa fa-video text-success"></i><span>40 Lectures</span></li>
									</ul>
								</div>
								<div class="preview_crs_info">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width:60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
							<div class="crs_grid_foot">
								<div class="crs_flex">
									<div class="crs_fl_first">
										<div class="crs_tutor">
											<div class="crs_tutor_thumb"><a href="instructor-detail.html"><img src="assets/img/user-4.jpg" class="img-fluid circle" alt="" /></a></div>
										</div>
									</div>
									<div class="crs_fl_last">
										<div class="foot_list_info">
											<ul>
												<li><div class="elsio_ic"><i class="fa fa-user text-danger"></i></div><div class="elsio_tx">4.7k</div></li>
												<li><div class="elsio_ic"><i class="fa fa-eye text-success"></i></div><div class="elsio_tx">42.4k</div></li>
												<li><div class="elsio_ic"><i class="fa fa-star text-warning"></i></div><div class="elsio_tx">4.7</div></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<!-- Single Item -->
					<div class="lios_item">
						<div class="crs_grid shadow_none brd">
							<div class="crs_grid_thumb">
								<a href="course-detail.html" class="crs_detail_link">
									<img src="assets/img/cr-5.jpg" class="img-fluid rounded" alt="" />
								</a>
								<div class="crs_video_ico">
									<i class="fa fa-play"></i>
								</div>
								<div class="crs_locked_ico">
									<i class="fa fa-lock"></i>
								</div>
							</div>
							<div class="crs_grid_caption">
								<div class="crs_flex">
									<div class="crs_fl_first">
										<div class="crs_cates cl_3"><span>Business</span></div>
									</div>
									<div class="crs_fl_last">
										<div class="crs_price"><h2><span class="currency">$</span><span class="theme-cl">129</span></h2></div>
									</div>
								</div>
								<div class="crs_title"><h4><a href="course-detail.html" class="crs_title_link">The complete business plan course includes 50 templates</a></h4></div>
								<div class="crs_info_detail">
									<ul>
										<li><i class="fa fa-clock text-danger"></i><span>06 hr 07 min</span></li>
										<li><i class="fa fa-video text-success"></i><span>35 Lectures</span></li>
									</ul>
								</div>
								<div class="preview_crs_info">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width:80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
							<div class="crs_grid_foot">
								<div class="crs_flex">
									<div class="crs_fl_first">
										<div class="crs_tutor">
											<div class="crs_tutor_thumb"><a href="instructor-detail.html"><img src="assets/img/user-3.jpg" class="img-fluid circle" alt="" /></a></div>
										</div>
									</div>
									<div class="crs_fl_last">
										<div class="foot_list_info">
											<ul>
												<li><div class="elsio_ic"><i class="fa fa-user text-danger"></i></div><div class="elsio_tx">4.7k</div></li>
												<li><div class="elsio_ic"><i class="fa fa-eye text-success"></i></div><div class="elsio_tx">42.4k</div></li>
												<li><div class="elsio_ic"><i class="fa fa-star text-warning"></i></div><div class="elsio_tx">4.7</div></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<!-- Single Item -->
					<div class="lios_item">
						<div class="crs_grid shadow_none brd">
							<div class="crs_grid_thumb">
								<a href="course-detail.html" class="crs_detail_link">
									<img src="assets/img/cr-6.jpg" class="img-fluid rounded" alt="" />
								</a>
								<div class="crs_video_ico">
									<i class="fa fa-play"></i>
								</div>
								<div class="crs_locked_ico">
									<i class="fa fa-lock"></i>
								</div>
							</div>
							<div class="crs_grid_caption">
								<div class="crs_flex">
									<div class="crs_fl_first">
										<div class="crs_cates cl_2"><span>Physics</span></div>
									</div>
									<div class="crs_fl_last">
										<div class="crs_price"><h2><span class="currency">$</span><span class="theme-cl">399</span></h2></div>
									</div>
								</div>
								<div class="crs_title"><h4><a href="course-detail.html" class="crs_title_link">Full web designing course with 20 web template designing</a></h4></div>
								<div class="crs_info_detail">
									<ul>
										<li><i class="fa fa-clock text-danger"></i><span>03 hr 10 min</span></li>
										<li><i class="fa fa-video text-success"></i><span>19 Lectures</span></li>
									</ul>
								</div>
								<div class="preview_crs_info">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width:75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
							<div class="crs_grid_foot">
								<div class="crs_flex">
									<div class="crs_fl_first">
										<div class="crs_tutor">
											<div class="crs_tutor_thumb"><a href="instructor-detail.html"><img src="assets/img/user-2.jpg" class="img-fluid circle" alt="" /></a></div>
										</div>
									</div>
									<div class="crs_fl_last">
										<div class="foot_list_info">
											<ul>
												<li><div class="elsio_ic"><i class="fa fa-user text-danger"></i></div><div class="elsio_tx">4.7k</div></li>
												<li><div class="elsio_ic"><i class="fa fa-eye text-success"></i></div><div class="elsio_tx">42.4k</div></li>
												<li><div class="elsio_ic"><i class="fa fa-star text-warning"></i></div><div class="elsio_tx">4.7</div></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<!-- Single Item -->
					<div class="lios_item">
						<div class="crs_grid shadow_none brd">
							<div class="crs_grid_thumb">
								<a href="course-detail.html" class="crs_detail_link">
									<img src="assets/img/cr-7.jpg" class="img-fluid rounded" alt="" />
								</a>
								<div class="crs_video_ico">
									<i class="fa fa-play"></i>
								</div>
								<div class="crs_locked_ico">
									<i class="fa fa-lock"></i>
								</div>
							</div>
							<div class="crs_grid_caption">
								<div class="crs_flex">
									<div class="crs_fl_first">
										<div class="crs_cates cl_1"><span>Design</span></div>
									</div>
									<div class="crs_fl_last">
										<div class="crs_price"><h2><span class="currency">$</span><span class="theme-cl">89</span></h2></div>
									</div>
								</div>
								<div class="crs_title"><h4><a href="course-detail.html" class="crs_title_link">Sociology Optional: Test Series for UPSC CSE Mains (2021 & 2022)</a></h4></div>
								<div class="crs_info_detail">
									<ul>
										<li><i class="fa fa-clock text-danger"></i><span>06 hr 07 min</span></li>
										<li><i class="fa fa-video text-success"></i><span>27 Lectures</span></li>
									</ul>
								</div>
								<div class="preview_crs_info">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width:35%" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
							<div class="crs_grid_foot">
								<div class="crs_flex">
									<div class="crs_fl_first">
										<div class="crs_tutor">
											<div class="crs_tutor_thumb"><a href="instructor-detail.html"><img src="assets/img/user-1.jpg" class="img-fluid circle" alt="" /></a></div>
										</div>
									</div>
									<div class="crs_fl_last">
										<div class="foot_list_info">
											<ul>
												<li><div class="elsio_ic"><i class="fa fa-user text-danger"></i></div><div class="elsio_tx">4.7k</div></li>
												<li><div class="elsio_ic"><i class="fa fa-eye text-success"></i></div><div class="elsio_tx">42.4k</div></li>
												<li><div class="elsio_ic"><i class="fa fa-star text-warning"></i></div><div class="elsio_tx">4.7</div></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				</div>
			</div>
		</div>
		
	</div>
</section>

<section class="min">
	<div class="container">
	
		<div class="row justify-content-center">
			<div class="col-lg-7 col-md-10 text-center">
				<div class="sec-heading center mb-4">
					<h2>Explore Top <span class="theme-cl">Categories</span></h2>
					<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores</p>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="edu_cat_2 cat-1">
					<div class="edu_cat_icons">
						<a class="pic-main" href="#"><img src="<?= site_url(); ?>public/images/content.png" class="img-fluid" alt="" /></a>
					</div>
					<div class="edu_cat_data">
						<h4 class="title"><a href="#">Development</a></h4>
						<ul class="meta">
							<li class="video"><i class="ti-video-clapper"></i>23 Classes</li>
						</ul>
					</div>
				</div>							
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="edu_cat_2 cat-2">
					<div class="edu_cat_icons">
						<a class="pic-main" href="#"><img src="<?= site_url(); ?>public/images/briefcase.png" class="img-fluid" alt="" /></a>
					</div>
					<div class="edu_cat_data">
						<h4 class="title"><a href="#">Business</a></h4>
						<ul class="meta">
							<li class="video"><i class="ti-video-clapper"></i>58 Classes</li>
						</ul>
					</div>
				</div>							
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="edu_cat_2 cat-3">
					<div class="edu_cat_icons">
						<a class="pic-main" href="#"><img src="<?= site_url(); ?>public/images/career.png" class="img-fluid" alt="" /></a>
					</div>
					<div class="edu_cat_data">
						<h4 class="title"><a href="#">Accounting</a></h4>
						<ul class="meta">
							<li class="video"><i class="ti-video-clapper"></i>74 Classes</li>
						</ul>
					</div>
				</div>							
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="edu_cat_2 cat-4">
					<div class="edu_cat_icons">
						<a class="pic-main" href="#"><img src="<?= site_url(); ?>public/images/python.png" class="img-fluid" alt="" /></a>
					</div>
					<div class="edu_cat_data">
						<h4 class="title"><a href="#">IT & Software</a></h4>
						<ul class="meta">
							<li class="video"><i class="ti-video-clapper"></i>65 Classes</li>
						</ul>
					</div>
				</div>							
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="edu_cat_2 cat-10">
					<div class="edu_cat_icons">
						<a class="pic-main" href="#"><img src="<?= site_url(); ?>public/images/designer.png" class="img-fluid" alt="" /></a>
					</div>
					<div class="edu_cat_data">
						<h4 class="title"><a href="#">Art & Design</a></h4>
						<ul class="meta">
							<li class="video"><i class="ti-video-clapper"></i>43 Classes</li>
						</ul>
					</div>
				</div>							
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="edu_cat_2 cat-6">
					<div class="edu_cat_icons">
						<a class="pic-main" href="#"><img src="<?= site_url(); ?>public/images/speaker.png" class="img-fluid" alt="" /></a>
					</div>
					<div class="edu_cat_data">
						<h4 class="title"><a href="#">Marketing</a></h4>
						<ul class="meta">
							<li class="video"><i class="ti-video-clapper"></i>82 Classes</li>
						</ul>
					</div>
				</div>							
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="edu_cat_2 cat-7">
					<div class="edu_cat_icons">
						<a class="pic-main" href="#"><img src="<?= site_url(); ?>public/images/photo.png" class="img-fluid" alt="" /></a>
					</div>
					<div class="edu_cat_data">
						<h4 class="title"><a href="#">Photography</a></h4>
						<ul class="meta">
							<li class="video"><i class="ti-video-clapper"></i>25 Classes</li>
						</ul>
					</div>
				</div>							
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="edu_cat_2 cat-8">
					<div class="edu_cat_icons">
						<a class="pic-main" href="#"><img src="<?= site_url(); ?>public/images/yoga.png" class="img-fluid" alt="" /></a>
					</div>
					<div class="edu_cat_data">
						<h4 class="title"><a href="#">Health & Fitness</a></h4>
						<ul class="meta">
							<li class="video"><i class="ti-video-clapper"></i>43 Classes</li>
						</ul>
					</div>
				</div>							
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="edu_cat_2 cat-9">
					<div class="edu_cat_icons">
						<a class="pic-main" href="#"><img src="<?= site_url(); ?>public/images/health.png" class="img-fluid" alt="" /></a>
					</div>
					<div class="edu_cat_data">
						<h4 class="title"><a href="#">Lifestyle</a></h4>
						<ul class="meta">
							<li class="video"><i class="ti-video-clapper"></i>38 Classes</li>
						</ul>
					</div>
				</div>							
			</div>
		</div>
		
	</div>
</section>

<section class="gray">
	<div class="container">
	
		<div class="row justify-content-center">
			<div class="col-lg-7 col-md-8">
				<div class="sec-heading center">
					<h2>Our Students <span class="theme-cl">Reviews</span></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
				</div>
			</div>
		</div>
		
		<div class="row justify-content-center">
			<div class="col-xl-12 col-lg-12 col-sm-12">
				
				<div class="reviews-slide space">
					
					<!-- Single Item -->
					<div class="single_items lios_item">
						<div class="_testimonial_wrios shadow_none">
							<div class="_testimonial_flex">
								<div class="_testimonial_flex_first">
									<div class="_tsl_flex_thumb">
										<img src="assets/img/user-1.jpg" class="img-fluid" alt="">
									</div>
									<div class="_tsl_flex_capst">
										<h5>Susan D. Murphy</h5>
										<div class="_ovr_posts"><span>CEO, Leader</span></div>
										<div class="_ovr_rates"><span><i class="fa fa-star"></i></span>4.7</div>
									</div>
								</div>
								<div class="_testimonial_flex_first_last">
									<div class="_tsl_flex_thumb">
										<img src="assets/img/c-1.png" class="img-fluid" alt="">
									</div>
								</div>
							</div>
							
							<div class="facts-detail">
								<p>Faucibus tristique felis potenti ultrices ornare rhoncus semper hac facilisi Rutrum tellus lorem sem velit nisi non pharetra in dui.</p>
							</div>
						</div>
					</div>
					
					<!-- Single Item -->
					<div class="single_items lios_item">
						<div class="_testimonial_wrios shadow_none">
							<div class="_testimonial_flex">
								<div class="_testimonial_flex_first">
									<div class="_tsl_flex_thumb">
										<img src="assets/img/user-2.jpg" class="img-fluid" alt="">
									</div>
									<div class="_tsl_flex_capst">
										<h5>Maxine E. Gagliardi</h5>
										<div class="_ovr_posts"><span>Apple CEO</span></div>
										<div class="_ovr_rates"><span><i class="fa fa-star"></i></span>4.5</div>
									</div>
								</div>
								<div class="_testimonial_flex_first_last">
									<div class="_tsl_flex_thumb">
										<img src="assets/img/c-2.png" class="img-fluid" alt="">
									</div>
								</div>
							</div>
							
							<div class="facts-detail">
								<p>Faucibus tristique felis potenti ultrices ornare rhoncus semper hac facilisi Rutrum tellus lorem sem velit nisi non pharetra in dui.</p>
							</div>
						</div>
					</div>
					
					<!-- Single Item -->
					<div class="single_items lios_item">
						<div class="_testimonial_wrios shadow_none">
							<div class="_testimonial_flex">
								<div class="_testimonial_flex_first">
									<div class="_tsl_flex_thumb">
										<img src="assets/img/user-3.jpg" class="img-fluid" alt="">
									</div>
									<div class="_tsl_flex_capst">
										<h5>Roy M. Cardona</h5>
										<div class="_ovr_posts"><span>Google Founder</span></div>
										<div class="_ovr_rates"><span><i class="fa fa-star"></i></span>4.9</div>
									</div>
								</div>
								<div class="_testimonial_flex_first_last">
									<div class="_tsl_flex_thumb">
										<img src="assets/img/c-3.png" class="img-fluid" alt="">
									</div>
								</div>
							</div>
							
							<div class="facts-detail">
								<p>Faucibus tristique felis potenti ultrices ornare rhoncus semper hac facilisi Rutrum tellus lorem sem velit nisi non pharetra in dui.</p>
							</div>
						</div>
					</div>
					
					<!-- Single Item -->
					<div class="single_items lios_item">
						<div class="_testimonial_wrios shadow_none">
							<div class="_testimonial_flex">
								<div class="_testimonial_flex_first">
									<div class="_tsl_flex_thumb">
										<img src="assets/img/user-4.jpg" class="img-fluid" alt="">
									</div>
									<div class="_tsl_flex_capst">
										<h5>Dorothy K. Shipton</h5>
										<div class="_ovr_posts"><span>Linkedin Leader</span></div>
										<div class="_ovr_rates"><span><i class="fa fa-star"></i></span>4.7</div>
									</div>
								</div>
								<div class="_testimonial_flex_first_last">
									<div class="_tsl_flex_thumb">
										<img src="assets/img/c-4.png" class="img-fluid" alt="">
									</div>
								</div>
							</div>
							
							<div class="facts-detail">
								<p>Faucibus tristique felis potenti ultrices ornare rhoncus semper hac facilisi Rutrum tellus lorem sem velit nisi non pharetra in dui.</p>
							</div>
						</div>
					</div>
					
					<!-- Single Item -->
					<div class="single_items lios_item">
						<div class="_testimonial_wrios shadow_none">
							<div class="_testimonial_flex">
								<div class="_testimonial_flex_first">
									<div class="_tsl_flex_thumb">
										<img src="assets/img/user-5.jpg" class="img-fluid" alt="">
									</div>
									<div class="_tsl_flex_capst">
										<h5>Robert P. McKissack</h5>
										<div class="_ovr_posts"><span>CEO, Leader</span></div>
										<div class="_ovr_rates"><span><i class="fa fa-star"></i></span>4.7</div>
									</div>
								</div>
								<div class="_testimonial_flex_first_last">
									<div class="_tsl_flex_thumb">
										<img src="assets/img/c-5.png" class="img-fluid" alt="">
									</div>
								</div>
							</div>
							
							<div class="facts-detail">
								<p>Faucibus tristique felis potenti ultrices ornare rhoncus semper hac facilisi Rutrum tellus lorem sem velit nisi non pharetra in dui.</p>
							</div>
						</div>
					</div>
				
				</div>
			
			</div>
		</div>
		
	</div>
</section>


<section class="theme-bg call_action_wrap-wrap">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
				<div class="call_action_wrap">
					<div class="call_action_wrap-head">
						<h3>Do You Have Questions ?</h3>
						<span>We'll help you to grow your career and growth.</span>
					</div>
					<a href="#" class="btn btn-call_action_wrap">Contact Us Today</a>
				</div>
				
			</div>
		</div>
	</div>
</section>