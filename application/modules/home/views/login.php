<script src="https://apis.google.com/js/client:platform.js?onload=renderButton" async defer></script>
<script async defer src="https://apis.google.com/js/api.js" onload="this.onload=function(){};HandleGoogleApiLibrary()" onreadystatechange="if (this.readyState === 'complete') this.onload()"></script>
<section>
	<div class="container">
		<div class="row justify-content-center">
		
			<div class="col-xl-7 col-lg-8 col-md-12 col-sm-12">
				<form method="POST" id="frmLogin">
					<div class="crs_log_wrap">
						<div class="crs_log__thumb">
							<img src="<?= site_url('public/images/banner-2.jpg'); ?>" class="img-fluid" alt="" />
						</div>
						<div class="crs_log__caption">
							<div class="rcs_log_123">
								<div class="rcs_ico"><i class="fas fa-lock"></i></div>
							</div>
							
							<div class="rcs_log_124">
								<div class="Lpo09"><h4>Login Your Account</h4></div>
								<div class="form-group">
									<label>User Name</label>
									<div class="input-with-icon">
										<input type="email" name="username" id="username" class="form-control" placeholder="Email">
										<i class="ti-user"></i>
									</div>
								</div>
								
								<div class="form-group">
									<label>Password</label>
									<div class="input-with-icon">
										<input type="password" class="form-control" name="password" id="password" placeholder="*******">
										<i class="ti-unlock"></i>
									</div>
								</div>
								
								<div class="form-group">
									<button type="submit" id="btn-login" class="btn btn-md full-width theme-bg text-white">Login</button>
								</div>
							</div>
							<div class="rcs_log_125">
								<span>Or Login with Social Info</span>
							</div>
							<div class="rcs_log_126 full">
								<ul class="social_log_45 row">
									<li class="col-xl-4 col-lg-4 col-md-4 col-4"><a href="javascript:void(0);" class="sl_btn"><i class="ti-facebook text-info"></i>Facebook</a></li>
									<li class="col-xl-4 col-lg-4 col-md-4 col-4"><a href="javascript:void(0);" id="btn-google" class="sl_btn"><i class="ti-google text-danger"></i>Google</a></li>
									<li class="col-xl-4 col-lg-4 col-md-4 col-4"><a href="javascript:void(0);" class="sl_btn"><i class="ti-mobile text-primary"></i>OTP Login</a></li>
								</ul>
								<button type="button" id="btnGoogle" style="display: none;">None</button>
							</div>
						</div>
						<div class="crs_log__footer d-flex justify-content-between">
							<div class="fhg_45"><p class="musrt">Don't have account? <a href="<?= site_url('register'); ?>" class="theme-cl">SignUp</a></p></div>
							<div class="fhg_45"><p class="musrt"><a href="<?= site_url('forget-password'); ?>" class="text-danger">Forgot Password?</a></p></div>
						</div>
					</div>
				</form>
			</div>

		</div>
	</div>
</section>

<script src="<?= site_url(); ?>public/js/common.min.js"></script>