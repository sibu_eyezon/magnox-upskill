<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    require_once "vendor/autoload.php";

    class Home extends MY_Controller{
        
        public function __construct(){
            parent::__construct();
            $this->load->module('settings');
            $this->load->module('homesetup');
            $this->load->model('mastermodel', 'mm');
        }

        public function index(){
            $data = array();
            $data['pageTitle'] = 'Home | Magnox Academy';
            $data['page_content'] = $this->load->view('home/index', null, TRUE);
            $this->homesetup->main($data);
        }

        /*==========================================================================================*/

        public function login($value='')
        {
            $data = array();
            $data['pageTitle'] = 'Login | Magnox Academy';
            // $clientDetails = $this->settings->getClient();
            // $content['gauth_url'] = $clientDetails['authurl'];
            $content['client_id'] = '794771726696-4u2o7uve4fkj3frusitt7m57cr2r1j49.apps.googleusercontent.com';
            $data['page_content'] = $this->load->view('home/login', $content, TRUE);
            $this->homesetup->main($data);
        }

        public function google_login($value='')
        {
            $res = array('status'=>false, 'msg'=>'Something went wrong.');
            // $userData = json_decode($_POST['userData']);
            // print_r($userData); exit;
            if(!empty($_POST['userId'])){
                $uauth['txt_google_id']  = $_POST['userId'];
                $udetails['first_name'] = $uauth['first_name'] = $_POST['first_name'];
                $udetails['last_name'] = $uauth['last_name']  = $_POST['last_name'];
                $udetails['txt_email'] = $uauth['txt_email']      = $_POST['email'];
                $udetails['profile_pic'] = $uauth['profile_pic'] = $_POST['image'];
                $uauth['yn_email_verified'] = 'Y';
                $uauth['update_datetime'] = $udetails['update_datetime'] = date('Y-m-d H:i:s');

                $getUser = $this->mm->find_user_by_email($_POST['email']);
                if(!empty($getUser)){
                    $status = $getUser[0]->yn_active;
                    if($status == 'Y'){
                        updateData('user_details', $udetails, 'id='.$getUser[0]->id);
                        updateData('user_auth', $uauth, 'user_id='.$getUser[0]->id);
                        $getUserRole = $this->mm->get_user_roles($getUser[0]->user_id);
                        $roles = array_map(function($roles){ return [$roles->rolename, $roles->status]; }, $getUserRole);
                        $userdata = array(
                            'userId'=>$getUser[0]->user_id,
                            'name'=>trim($uauth['first_name']." ".$uauth['last_name']),
                            'profile_img'=>$uauth['profile_pic'],
                            'roles'=>$roles,
                            'isLoggedIn'=>true,
                            'mobile_verified'=>$getUser[0]->yn_mobile_verified,
                            'email_verified'=>'Y'
                        );
                        $this->session->set_userdata('b2cData', $userdata);
                        $res['status'] = true;
                    }else if($status == 'B'){
                        $res['msg'] = 'Your account is blocked. Contact Admin to unblock it';
                    }else{
                        $res['msg'] = 'Your account is inactive. Contact Admin to activate';
                    }
                }else{
                    $uauth['yn_active'] = 'Y';
                    $uauth['yn_mobile_verified'] = 'N';
                    $uauth['yn_email_verified'] = 'Y';
                    $urole['stat_datetime'] = $urole['add_datetime'] = $uauth['add_datetime'] = $udetails['add_datetime'] = date('Y-m-d H:i:s');
                    $urole['role_id'] = $role[0]->id;
                    $urole['status'] = 'Y';

                    $user_id = setup_user_data($udetails, $uauth, $urole);
                    if($user_id){
                        $res['status'] = true;
                        $userdata = array(
                            'userId'=>$user_id,
                            'name'=>trim($uauth['first_name']." ".$uauth['last_name']),
                            'profile_img'=>$uauth['profile_pic'],
                            'roles'=>[0=>['Student', 'Y']],
                            'isLoggedIn'=>true,
                            'mobile_verified'=>'N',
                            'email_verified'=>'Y'
                        );
                        $this->session->set_userdata('b2cData', $userdata);
                    }else{
                        $res['msg'] = 'Server issue. Please try again.';
                    }
                }
            }else{
                $res['msg'] = 'Could not get your google details. Please try again.';
            }
            echo json_encode($res);
        }

        public function user_login()
        {
            $res = array('status'=>false, 'msg'=>'Something went wrong.');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('username', 'Username', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');

            if ($this->form_validation->run() == FALSE){
                $res = array('status'=>false, 'msg'=>validation_errors());
            }else{
                $email = strtolower(trim($_POST['username']));
                $password = trim($_POST['password']);

                $getUser = $this->mm->find_user_by_email($email);
                if(!empty($getUser)){
                    if(password_verify($password, trim($getUser[0]->password))){
                        $status = $getUser[0]->yn_active;
                        if($status == 'Y'){
                            $profile_dp = trim($getUser[0]->profile_pic);
                            $profile_dp = ($profile_dp != "")? ( (filter_var($profile_dp, FILTER_VALIDATE_URL))? $profile_dp : site_url($profile_dp) ) : site_url('public/images/default-avatar.png');

                            $getUserRole = $this->mm->get_user_roles($getUser[0]->user_id);
                            $roles = array_map(function($roles){ return [$roles->rolename, $roles->status]; }, $getUserRole);

                            $userdata = array(
                                'userId'=>$getUser[0]->user_id,
                                'name'=>trim($getUser[0]->first_name." ".$getUser[0]->last_name),
                                'profile_img'=>$profile_dp,
                                'roles'=>$roles,
                                'isLoggedIn'=>true,
                                'mobile_verified'=>$getUser[0]->yn_mobile_verified,
                                'email_verified'=>$getUser[0]->yn_email_verified
                            );
                            $this->session->set_userdata('b2cData', $userdata);
                            $res['status'] = true;
                        }else if($status == 'B'){
                            $res['msg'] = 'Your account is blocked. Contact Admin to unblock it';
                        }else{
                            $res['msg'] = 'Your account is inactive. Contact Admin to activate';
                        }
                    }else{
                        $res['msg'] = 'Wrong password entered.';
                    }
                }else{
                    $res['msg'] = 'User not found.';
                }
            }
            echo json_encode($res);
        }
        /*-------------------------------------------------------------------------------------------*/

        public function check_existing_email()
        {
            $return = 'This email already exist.';
            if(isset($_POST['useremail'])){
                $chkUser = $this->mm->check_unique_user("txt_email='".$_POST['useremail']."'");
                if($chkUser==0){
                    $return = true;
                }
            }
            echo json_encode($return);
            exit();
        }
        public function check_existing_mobile()
        {
            $return = 'This mobile already exist.';
            if(isset($_POST['userphone'])){
                $chkUser = $this->mm->check_unique_user("num_mobile='".$_POST['userphone']."'");
                if($chkUser==0){
                    $return = true;
                }
            }
            echo json_encode($return);
            exit();
        }

        public function register($value='')
        {
            $data = array();
            $data['pageTitle'] = 'Register | Magnox Academy';
            $clientDetails = $this->settings->getClient();
            $content['gauth_url'] = $clientDetails['authurl'];
            $data['page_content'] = $this->load->view('home/register', $content, TRUE);
            $this->homesetup->main($data);
        }

        public function user_register($value='')
        {
            $res = array('status'=>false, 'msg'=>'Something went wrong.');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('fname', 'Firstname', 'trim|required');
            $this->form_validation->set_rules('lname', 'Lastname', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
            $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|integer|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_rules('cnf_password', 'Confirm Password', 'trim|required|matches[password]');
            if ($this->form_validation->run() == FALSE){
                $res['msg'] = validation_errors();
            }else{
                $chkUser = $this->mm->check_unique_user("txt_email='".$_POST['email']."' OR num_mobile='".$_POST['mobile']."'");
                // $res = array('status'=>false, 'msg'=>$chkUser);
                if($chkUser == 0){
                    $role = $this->mm->get_role_id_by_name('Student');
                    $uauth['txt_email'] = $udetails['txt_email'] = strtolower(trim($_POST['email']));
                    $uauth['first_name'] = $udetails['first_name'] = ucwords(strtolower(trim($_POST['fname'])));
                    $uauth['last_name'] = $udetails['last_name'] = ucwords(strtolower(trim($_POST['lname'])));
                    $uauth['num_mobile'] = $udetails['num_mobile'] = trim($_POST['mobile']);
                    $uauth['password'] = password_hash(trim($_POST['password']), PASSWORD_BCRYPT);// $this->password->hash();
                    $uauth['yn_active'] = 'Y';
                    $uauth['yn_mobile_verified'] = 'N';
                    $uauth['yn_email_verified'] = 'N';
                    $urole['stat_datetime'] = $urole['add_datetime'] = $uauth['add_datetime'] = $udetails['add_datetime'] = date('Y-m-d H:i:s');
                    $urole['role_id'] = $role[0]->id;
                    $urole['status'] = 'Y';
                    if(setup_user_data($udetails, $uauth, $urole)){
                        $res['status'] = true;
                        $res['msg'] = 'You have successfully registered.';
                    }else{
                        $res['msg'] = 'Server issue. Please try again.';
                    }
                }else{
                    $res['msg'] = 'This user already exists.';
                }//check user condition
            }
            echo json_encode($res);
        }

        /*============================================================================================*/

        public function forget_password($value='')
        {
            $data = array();
            $data['pageTitle'] = 'Forget Password | Magnox Academy';
            $clientDetails = $this->settings->getClient();
            $content['gauth_url'] = $clientDetails['authurl'];
            $data['page_content'] = $this->load->view('home/forget-password', $content, TRUE);
            $this->homesetup->main($data);
        }

        public function user_forget_password()
        {
            $this->load->helper('string');
            $res = array('status'=>false, 'msg'=>"Something went wrong.", 'code'=>"", 'tokenId'=>"");
            if(isset($_POST['email'])){
                $email = strtolower(trim($_POST['email']));
                if($email){
                    $getUser = $this->mm->find_user_by_email($email);
                    if(!empty($getUser)){
                        if($getUser[0]->yn_active == 'Y'){
                            $code = random_string('alnum', 6);
                            $this->settings->mobile_otp_send(trim($getUser[0]->num_mobile), $code);
                            $this->settings->MailSystem($email, '', "Forget Password - OTP", "The OTP for changing to new password is ".$code);
                            $res['status'] = true;
                            $res['code'] = base64_encode($code);
                            $res['tokenId'] = base64_encode($getUser[0]->id);
                            $res['msg'] = 'OTP has been send to your email inbox/spam and mobile. Try again if you haven\'t received it.';
                        }else{
                            $res['msg'] = 'This email is blocked. Contact Administrator.';
                        }
                        
                    }else{
                        $res['msg'] = 'This email does not exist. Please register yourself.';
                    }
                }else{
                    $res['msg'] = 'Email must be entered.';
                }
            }else{
                $res['msg'] = 'No input found.';
            }
            echo json_encode($res);
        }

        public function user_change_password()
        {
            $res = array('status'=>false, 'msg'=>'Something went wrong.');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('otp', 'OTP', 'trim|required|min_length[6]|max_length[6]');
            $this->form_validation->set_rules('password', 'New Password', 'trim|required');
            $this->form_validation->set_rules('cnf_password', 'Confirm New Password', 'trim|required|matches[password]');

            if ($this->form_validation->run() == FALSE){
                $res['msg'] = validation_errors();
            }else{
                $uauth_id = trim($_POST['tokenId']);
                $code = trim($_POST['code']);
                $otp = trim($_POST['otp']);
                $password = trim($_POST['password']);

                if($uauth_id && $code){
                    if(base64_decode($code) === $otp){
                        $data['password'] = password_hash($password, PASSWORD_BCRYPT);
                        updateData('user_auth', $data, 'id='.base64_decode($uauth_id));
                        $res['status'] = true;
                    }else{
                        $res['msg'] = 'Wrong OTP entered.';
                    }
                }else{
                    $res['msg'] = 'Do not try to tamper with the code.';
                }
            }

            echo json_encode($res);
        }

    }//class ends
?>