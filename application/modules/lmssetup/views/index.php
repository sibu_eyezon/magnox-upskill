<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <meta name="google-site-verification" content="gf3lL_sBDA2EULmFEsLbF4k9dRACIbrS3Rni2s0M34Q" />
  <title><?= $pageTitle; ?></title>
  <link rel="stylesheet" href="<?= site_url(); ?>public/lms/css/app.min.css">
  <link rel="stylesheet" href="<?= site_url(); ?>public/lms/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?= site_url(); ?>public/lms/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?= site_url(); ?>public/lms/bundles/izitoast/css/iziToast.min.css">
  <link rel="stylesheet" href="<?= site_url(); ?>public/lms/css/style.css">
  <link rel="stylesheet" href="<?= site_url(); ?>public/lms/css/components.css">
  <link rel="stylesheet" href="<?= site_url(); ?>public/lms/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?= site_url(); ?>public/images/favicon.ico' />

  <script src="<?= site_url(); ?>public/lms/js/app.min.js"></script>
  <script src="<?= site_url(); ?>public/lms/bundles/izitoast/js/iziToast.min.js"></script>
  <script src="<?= site_url(); ?>public/lms/bundles/sweetalert/sweetalert.min.js"></script>
  <script src="<?= site_url(); ?>public/lms/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="<?= site_url(); ?>public/lms/bundles/datatables/datatables.min.js"></script>
  <script src="<?= site_url(); ?>public/lms/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script type="text/javascript">var baseURL = '<?= site_url(); ?>';</script>
</head>
<body>
  <div class="loader"></div>
  <script type="text/javascript">
    function show_pop_messages(msg, status){
      if(status){
        iziToast.success({
          title: 'Success!',
          message: msg,
          position: 'topRight'
        });
      }else{
        iziToast.error({
          title: 'Warning!',
          message: msg,
          position: 'topRight'
        });
      }
    }
  </script>
  <?php 
    if($this->session->flashdata('errors')!=""){
      echo '<script>show_pop_messages("'.$this->session->flashdata("errors").'", false);</script>';
      unset($_SESSION['errors']);
    }else if($this->session->flashdata('success')!=""){
      echo '<script>show_pop_messages("'.$this->session->flashdata("success").'", true);</script>';
      unset($_SESSION['success']);
    }
  ?>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
	<?php include 'topnav.php'; include 'sidebar.php'; ?>
	
	<!--MAIN CONTENT START-->
	<?php if(isset($page_content)){ echo $page_content; } ?>
	<!--MAIN CONTENT ENDS-->
	
	<?php include 'footer.php'; ?>
	</div>
  </div>
  
  <!-- JS Libraies -->
  <script src="<?= site_url(); ?>public/lms/bundles/jquery-pwstrength/jquery.pwstrength.min.js"></script>
  <script src="<?= site_url(); ?>public/lms/js/custom.js"></script>
  <script src="<?= site_url(); ?>public/lms/bundles/apexcharts/apexcharts.min.js"></script>
  <script src="<?= site_url(); ?>public/lms/bundles/jquery-ui/jquery-ui.min.js"></script>
  <script src="<?= site_url(); ?>public/lms/js/page/datatables.js"></script>
  <script src="<?= site_url(); ?>public/lms/js/page/index.js"></script>
  <script src="<?= site_url(); ?>public/lms/js/scripts.js"></script>
</body>
</html>