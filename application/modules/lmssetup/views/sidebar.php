<div class="main-sidebar sidebar-style-2">
	<aside id="sidebar-wrapper">
	  <div class="sidebar-brand">
		<a href="<?= site_url('dashboard'); ?>"> <img alt="image" src="<?= site_url(); ?>public/images/logo.png" class="header-logo" />
		</a>
	  </div>
	  <ul class="sidebar-menu">
<!-- 		<li class="menu-header">Main</li> -->
		<li class="dropdown active">
		  <a href="<?= site_url('dashboard'); ?>" class="nav-link"><i data-feather="grid"></i><span>Dashboard</span></a>
		</li>
		<li><a class="nav-link" href="<?= site_url('schedule'); ?>"><i data-feather="calendar"></i><span>Calendar</span></a></li>
		<li><a class="nav-link" href="#"><i data-feather="file-text"></i><span>Assignment</span></a></li>
		<li><a class="nav-link" href="#"><i data-feather="clipboard"></i><span>Examination</span></a></li>
		<li><a class="nav-link" href="#"><i data-feather="book-open"></i><span>Study Material</span></a></li>
		<li><a class="nav-link" href="<?= site_url('batches-list'); ?>"><i data-feather="tv"></i><span>Batches</span></a></li>
		<li><a class="nav-link" href="<?= site_url('notifications'); ?>"><i data-feather="message-circle"></i><span>Notice Board</span></a></li>
	  </ul>
	</aside>
</div>