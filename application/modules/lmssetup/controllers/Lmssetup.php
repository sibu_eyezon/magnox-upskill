<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Lmssetup extends MY_Controller{
        
        public function __construct(){
            parent::__construct();
        }

        public function lmsmain($data){
            $this->load->view('index', $data);
        }

    }//class ends
?>