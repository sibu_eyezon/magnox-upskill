<?php

defined('BASEPATH') OR exit('No direct script access allowed');


function insertData($tableName, $data){
	$ci = & get_instance();
	$ci->load->database();
	return $ci->db->insert($tableName, $data);
}
function insertBatchData($tableName, $data)
{
	$ci = & get_instance();
	$ci->load->database();
	return $ci->db->insert_batch($tableName, $data);
}
function insertDataRetId($tableName, $data){
	$ci = & get_instance();
	$ci->load->database();
	$ci->db->insert($tableName, $data);
	return $ci->db->insert_id();
}
function updateData($tableName, $data, $where){
	$ci = & get_instance();
	$ci->load->database();
	$ci->db->where($where);
	return $ci->db->update($tableName, $data);
}
function deleteData($tableName, $where){
	$ci = & get_instance();
	$ci->load->database();
	$ci->db->where($where);
	return $ci->db->delete($tableName);
}